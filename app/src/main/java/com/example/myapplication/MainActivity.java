package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void pasarRegistro(View view) {
        Intent pasar =new Intent( MainActivity.this,Registr.class);
        startActivity(pasar);
    }

    public void pasarIniciosesion(View view) {
        Intent pasarSesion = new Intent(MainActivity.this,Iniciar_sesion.class);
        startActivity(pasarSesion);
    }
}